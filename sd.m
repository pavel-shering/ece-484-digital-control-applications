%This code simulates a single-input single-out sampled-data control system.
%It needs the simulink file sd_simulink.mdl

clc;
close all;
clear all
s = tf('s');

%% Define Continuous-Time Plant

P = 1/(s^2+2*s+2);

%% Define Continuous-Time Controller

C = 2/s;  

%% Discretize Controller

T = 1;                        % Sampling period
z = tf('z', T);
Cdt = c2d(C,T,'tustin');        % Discretize Controller using bilinear

%% Continuous-Time Closed-loop Transfer Functions

G1 = P*C/(1+P*C);    % TF from r to y
G2 = C/(1+P*C);      % TF from r to u
 
%% Call Simulink to Simulate Step Response of Sampled-Data System

sim('sd_simulink');

%% Plot Step Response

% plot data
[Y, t] = step(G1, t_out(end)); % ct response
[U, ~] = step(G2, t_out(end)); % ct response

H = plot(t_out, Ysd,t, Y,  'linewidth', 2); % sampled-data response
grid on;
xlabel('Time (s)','interpreter','latex','FontSize',20);
ylabel('Plant Output $y(t)$','interpreter','latex','FontSize',20);
title('Step response');
l1 = legend(H, sprintf('Sampled data with T =%gs',T),'Continuous system');
set(l1,'position',[0.2 0.8 0.33 0.08],'FontSize',12)

% plot control signal
figure;
hold on
str = stairs(T*(0:1:size(Uk)-1)', Uk, 'linewidth', 2);
hold off
grid on;
xlabel('Time (s)','interpreter','latex','FontSize',20);
ylabel('Controller Output $u(t)$','interpreter','latex','FontSize',20);
l2 = legend(sprintf('Sampled data with T =%gs',T));
set(l2,'position',[0.2 0.8 0.33 0.05],'FontSize',12)