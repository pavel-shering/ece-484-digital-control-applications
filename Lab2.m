clear all; clc;
close all;

K_1 = -1.23;
tau = 0.026;
Pnum = [K_1];
Pden = [tau 1 0];

P = tf(Pnum, Pden);
os = 5/100; % 5% overshoot 
Ts_max = 0.5; %[s] 2% settling time 

z = sqrt(log(os/100)^2 / (log(os/100)^2 + pi^2)); % zeta
wn = 4 / (Ts_max * z); % >= than this value 

theta = acos(z)*180/pi() % angle threshold
-z*wn % threshold of the good region on the x axis 

% poles = [-20, -8, -12] % estimated poles
poles = [-30, -10+j*4, -10-j*4] % estimated poles

current_theta = atan(4/10)*180/pi()
% syms s
% p_des = (s-poles(1))*(s-poles(2))*(s-poles(3))
% expand(p_des)

C = pp(P, poles)
% plot(gear_angle);


 