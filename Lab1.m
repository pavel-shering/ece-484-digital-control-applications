clear all;
clc;
Kp = [-20, -25, -30, -35];
os = [8.37, 13.0, 15.82, 15.0];
Tp = [0.115, 0.110, 0.1, 0.095];

z = [];
wn = [];
tau = [];
K1 = [];

for i = 1:length(Kp)
    z(i) = sqrt(log(os(i)/100)^2 / (log(os(i)/100)^2 + pi^2))
end 

for i = 1:length(Kp)
    wn(i) = pi / (Tp(i)*sqrt(1-z(i)^2))
end


for i = 1:length(Kp)
    tau(i) = 1 / (2*z(i)*wn(i))
end

for i = 1:length(Kp)
   K1(i) = wn(i)^2 * tau(i) / Kp(i)  
end

z'
wn'
K1'
tau'

