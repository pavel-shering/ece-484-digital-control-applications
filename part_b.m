% Kp = -8;
% a = 20;
% b = 25;
Kp = -21.5627;
a = 11.3722;
b = 81.5393;

% (21.5627 (s - 11.3722))/(s + 81.5393)
% (21.8539 (s - 9.89054))/(s + 81.5393)

K_1 = -1.23;
tau = 0.026;

s = tf('s');
C = Kp*((s+a)/(s+b));
P = K_1 / (s*(tau*s + 1));
G = P*C / (1 + P*C);

bw = bandwidth(G);
bode (G);

bw = (bw / (2*pi));

sample_freq = 25*bw;
sample_period = 1/sample_freq;

C_d = c2d (C, sample_period, 'tustin');

[num, den] = tfdata(C_d,'v');